import {Game} from "./Game";
import * as PIXI from 'pixi.js';
import {Points} from "./Points";

export class ProgressBar {
    private static instance: ProgressBar;
    private app: any;
    private minPoints: number;

    constructor() {
        this.app = Game.getApp();
        this.minPoints = 0;
    }

    public static getInstance(): ProgressBar {
        if (!ProgressBar.instance) {
            ProgressBar.instance = new ProgressBar();
        }
        return ProgressBar.instance;
    }

    create(minPoints: number) {
        this.minPoints = minPoints;
        const container = new PIXI.Container();
        this.app.stage.addChild(container);
        let texture = PIXI.Texture.from('/assets/images/rounded_rectangle_5.png');
        let gamePlace = new PIXI.Sprite(texture);
        gamePlace.width = 700;
        gamePlace.height = 80;
        gamePlace.x = 450;
        gamePlace.y = 0;
        container.addChild(gamePlace);

        texture = PIXI.Texture.from('/assets/images/progress.png');
        gamePlace = new PIXI.Sprite(texture);
        gamePlace.width = 100;
        gamePlace.height = 20;
        gamePlace.x = 750;
        gamePlace.y = 10;
        container.addChild(gamePlace);
    }

    listener() {
        return new Promise((resolve, reject) => {
            let interval = setInterval(() => {
                if (Points.getInstance().get() > this.minPoints) {
                    clearInterval(interval);
                    resolve(true);
                }
            }, 1000)
        })
    }
}