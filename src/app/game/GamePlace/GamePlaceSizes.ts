import {Sizes} from "./Interfaces/Sizes";

export class GamePlaceSizes implements Sizes
{
    private readonly CONTAINER_MAX_WIDTH: number = 1280;
    private readonly CONTAINER_PADDING: number = 150;
    private sizes: any;

    constructor() {
        let width = this.makeGamePlaceWidth();
        let height = width;
        let offsetX = this.makeGamePlaceOffsetX();
        let offsetY = this.makeGamePlaceOffsetY(height);
        this.sizes = {
            width: Math.round(width),
            height: Math.round(height),
            offsetX: Math.round(offsetX),
            offsetY: Math.round(offsetY)
        }
    }

    getSizes(): any {
        return this.sizes;
    }

    private makeGamePlaceWidth() {
        if(window.innerWidth > this.CONTAINER_MAX_WIDTH) {
            return (this.CONTAINER_MAX_WIDTH / 2) - this.CONTAINER_PADDING;
        }
        return (window.innerWidth - this.CONTAINER_PADDING)/2;
    }


    private makeGamePlaceOffsetX() {
        if(window.innerWidth > this.CONTAINER_MAX_WIDTH) {
            return (window.innerWidth - this.CONTAINER_MAX_WIDTH) / 2;
        }
        return this.CONTAINER_PADDING;
    }


    private makeGamePlaceOffsetY(height: number) {
        return (window.innerHeight - height) / 2;
    }
}