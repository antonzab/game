import * as PIXI from 'pixi.js';
import {GamePlaceSizes} from "./GamePlaceSizes";
import {Sizes} from "./Interfaces/Sizes";
import {Game} from "../Game";

export class GamePlace {

    private app: any;
    private gamePlaceSizes: Sizes;

    constructor() {
        this.app = Game.getApp();
        this.gamePlaceSizes = new GamePlaceSizes();
    }

    create() {
        let sizes: any = this.gamePlaceSizes.getSizes();

        const container = new PIXI.Container();
        this.app.stage.addChild(container);
        const texture = PIXI.Texture.from('/assets/images/rounded_rectangle_1_2.png');
        const gamePlace = new PIXI.Sprite(texture);
        gamePlace.width = sizes.width;
        gamePlace.height = sizes.height;
        gamePlace.x = sizes.offsetX;
        gamePlace.y = sizes.offsetY;
        container.addChild(gamePlace);
    }
}