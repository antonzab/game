import {Game} from "./Game";
import * as PIXI from 'pixi.js';

export class Time {
    private static instance: Time;
    private time: number;
    private app: any;
    private timePixiItem: any;
    private runned: boolean;

    private constructor() {
        this.app = Game.getApp();
        this.runned = false;
    }

    public static getInstance(): Time {
        if (!Time.instance) {
            Time.instance = new Time();
        }
        return Time.instance;
    }

    setTime(time: number) {
        this.time = time;
    }

    listener() {
        return new Promise((resolve)=>{
            let interval = setInterval(()=>{
                if(this.time <= 0 ) {
                    clearInterval(interval);
                    resolve(true);
                }
            },1000)
        })
    }

    create() {
        const timePixiItem = new PIXI.Text(`Время:  ${this.time}`);
        timePixiItem.x = 1200;
        timePixiItem.y = 220;
        this.timePixiItem = timePixiItem;
        this.app.stage.addChild(timePixiItem);
    }

    run() {
        this.runned = true;
        let interval = setInterval(() => {
            if (this.time == 0) {
                this.timePixiItem.text = `Время закончилось`;
                clearInterval(interval);
                return false;
            }
            this.time--;
            this.timePixiItem.text = `Время:  ${this.time}`;
        }, 1000)
    }
}