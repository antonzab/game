import * as PIXI from 'pixi.js';

export class Game {

    private static app: any;

    constructor() {
        Game.app = new PIXI.Application({
            width: window.innerWidth,
            height: window.innerHeight,
            sharedTicker: true,
            transparent: true
        })
    }

    init() {
        Game.app.renderer.backgroundColor = 0xffffff;
        document.body.appendChild(Game.app.view);
    }

    static getApp(): object {
        return Game.app;
    }
}