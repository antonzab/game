import {TailignCollection} from "./Tailing/TailignCollection";
import {Tailing} from "./Tailing/Tailing";
import {TailingFactory} from "./Tailing/TailingFactory";
import {TailingAnimation} from "../animations/TailingAnimation";

export class FillSpace {
    private isEndOperation: boolean = false;
    private countCollectionEndAnimtaions: number;

    execute() {
        this.checkFirstLine();
        return new Promise((resolve) => {
            let interval = setInterval(() => {
                if (this.isEndOperation) {
                    clearInterval(interval);
                    resolve(true);
                }
            }, 50);
        });
    }

    private checkFirstLine() {

        let collection = TailignCollection.getInstance().get();
        let firsLine: Tailing[] = collection.filter((tailing) => tailing.coordinate[1] == 0);

        for (let x = 0; x < TailingFactory.grid.x; x++) {
            let index = firsLine.findIndex((tailing) => tailing.coordinate[0] == x);
            if (index == -1) {
                let tailing = new TailingFactory().makeTailing(x, 0);
                TailignCollection.getInstance().add(tailing);
            }
        }

        if (TailignCollection.getInstance().get().length < TailingFactory.grid.x * TailingFactory.grid.y) {
            this.animateCollection().then(() => {
                this.checkFirstLine();
            });
        } else {
            this.isEndOperation = true;
        }
    }

    private animateCollection() {
        this.collectionAnimationExecute()
        let collectionLength = TailignCollection.getInstance().get().length;
        return new Promise((resolve) => {
            let interval = setInterval(() => {
                if (this.countCollectionEndAnimtaions == collectionLength) {
                    clearInterval(interval);
                    resolve(true);
                }
            }, 50);
        });
    }

    private collectionAnimationExecute() {

        let collection = TailignCollection.getInstance();
        this.countCollectionEndAnimtaions = 0;

        collection.get().sort((item, nextItem) => {
            return nextItem.coordinate[0] - item.coordinate[0]
        }).sort((item, nextItem) => {
            return nextItem.coordinate[1] - item.coordinate[1]
        }).forEach((tailing) => {
            let animate = new TailingAnimation(tailing);
            animate.drop().then(()=>{
                this.countCollectionEndAnimtaions++;
            });
        });
    }
}