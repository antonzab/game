import {Tailing} from "./Tailing";
import {TailignCollection} from "./TailignCollection";
import {GamePlayConfig} from "../../config/GamePlayConfig";
import {TailingAnimation} from "../../animations/TailingAnimation";

export class GroupTailing {

    private currentTailing: Tailing;
    private groupSize: number;
    private deletingEnd: boolean;

    constructor(tailing: Tailing) {
        this.currentTailing = tailing;
        this.deletingEnd = false;
        this.groupSize = 0;
    }

    getGroupSize(): number {
        return this.groupSize;
    }

    delete() {
        let group = this.getGroup();
        if (group.length >= GamePlayConfig.minTailingInGroup) {
            group.forEach((tailing) => {
                new TailingAnimation(tailing).delete().then(() => {
                    let collection = TailignCollection.getInstance();
                    collection.remove(tailing);
                    tailing.delete();
                })
            })
            new TailingAnimation(this.currentTailing).delete().then(() => {
                let collection = TailignCollection.getInstance();
                collection.remove(this.currentTailing);
                this.currentTailing.delete();
                this.deletingEnd = true;
            })
            this.groupSize = group.length;
        }
        else {
            this.deletingEnd = true;
        }

        return new Promise((resolve) => {
            let interval = setInterval(() => {
                if (this.deletingEnd) {
                    clearInterval(interval);
                    resolve(true);
                }
            }, 5);
        });
    }

    private getGroup(): Tailing[] {
        let tailing: Tailing[] = [this.currentTailing];
        let siblings = this.getSiblings(this.currentTailing);
        let i = 1; //первый у нас текущий элемент, пропускаю его.
        while (siblings.length > 0) {
            tailing = this.concatCollection(tailing, siblings);
            siblings = this.getSiblings(tailing[i]);
            i++;
        }
        return tailing;
    }

    private getSiblings(currentTailing: Tailing) {
        if (currentTailing == undefined) {
            return [];
        }
        let collection = TailignCollection.getInstance();

        return collection.get().filter((tailing) => {
            let isXsiblings = (
                tailing.coordinate[0] == currentTailing.coordinate[0] + 1 ||
                tailing.coordinate[0] == currentTailing.coordinate[0] - 1
            ) && tailing.coordinate[1] == currentTailing.coordinate[1];
            let isYsiblings = (
                tailing.coordinate[1] == currentTailing.coordinate[1] + 1 ||
                tailing.coordinate[1] == currentTailing.coordinate[1] - 1
            ) && tailing.coordinate[0] == currentTailing.coordinate[0];
            let siblings = isXsiblings || isYsiblings;
            return siblings && currentTailing.color == tailing.color && tailing.id != currentTailing.id;
        });
    }

    private concatCollection(tailings: Tailing[], siblings: Tailing[]): Tailing[] {
        let siblingsAllowed = siblings.filter((item) => {
            let searched = tailings.find((search) => {
                return search.id == item.id;
            })
            return searched === undefined;
        });
        return siblingsAllowed.length > 0 ? tailings.concat(siblingsAllowed) : tailings;
    }
}