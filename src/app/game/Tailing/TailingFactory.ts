import {Tailing} from "./Tailing";
import {TailignCollection} from "./TailignCollection";
import {GamePlaceSizes} from "../GamePlace/GamePlaceSizes";
import {Game} from "../Game";
import {TailingAnimation} from "../../animations/TailingAnimation";
import {GamePlayConfig} from "../../config/GamePlayConfig";

export class TailingFactory {

    container: any;
    app: any;
    static grid: { x: number, y: number };

    constructor() {
        let gamePlaceSizes = new GamePlaceSizes();
        this.container = gamePlaceSizes.getSizes();
        this.app = Game.getApp();
        TailingFactory.grid = {x: GamePlayConfig.gamePlaceSizeWidth, y: GamePlayConfig.gamePlaceSizeHeight};
    }

    makeTailing(x: number, y: number): Tailing {
        let tailing = new Tailing();
        let itemSize = this.getItemSize();
        itemSize.offsetY += itemSize.height * y;
        itemSize.offsetX += itemSize.width * x;
        tailing.setSize(itemSize);
        tailing.id = this.makeTailingId();
        tailing.coordinate = [x, y];
        tailing.build();
        const animate = new TailingAnimation(tailing);
        animate.build();
        return tailing;
    }

    private makeTailingId() {
        let collection = TailignCollection.getInstance().get();
        let id: number = 0;
        if (collection.length > 0) {
            id = collection.sort((a, b) => {
                return a.id - b.id
            })[collection.length - 1].id;
        }
        return ++id;
    }

    generate() {
        for (let y = 0; y < TailingFactory.grid.y; y++) {
            for (let x = 0; x < TailingFactory.grid.x; x++) {
                let tailing = this.makeTailing(x, y);
                TailignCollection.getInstance().add(tailing);
            }
        }
    }

    private getItemSize() {
        let width = this.container.width / TailingFactory.grid.x
        let height = this.container.height / TailingFactory.grid.y;
        let padding = 15;
        return {
            'offsetX': Math.round(this.container.offsetX + padding),
            'offsetY': Math.round(this.container.offsetY + padding),
            'width': Math.round(width - 2 * padding / TailingFactory.grid.x),
            'height': Math.round(height - 2 * padding / TailingFactory.grid.y)
        }
    }
}