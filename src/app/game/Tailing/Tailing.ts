import * as PIXI from "pixi.js";
import {Helper} from "../../helpers/Helper";
import {Actions} from "../../actions/Actions";
import {Game} from "../Game";

export class Tailing {

    private app: any
    private offsetX: number;
    private offsetY: number;
    private containerWidth: number;
    private containerHeight: number;
    private gameItem: object;
    private pixiContainer: any;
    coordinate: [number, number];
    color: string;
    id: number

    constructor() {
        this.app = Game.getApp();
    }

    setSize(size: any) {
        this.offsetX = Math.round(size.offsetX);
        this.offsetY = Math.round(size.offsetY);
        this.containerWidth = Math.round(size.width);
        this.containerHeight = Math.round(size.height);
    }

    build() {
        const container = new PIXI.Container();
        this.pixiContainer = container;
        this.app.stage.addChild(container);
        const texture = this.getRandomTexture();
        const gameItem = new PIXI.Sprite(texture);
        gameItem.buttonMode = true;
        gameItem.interactive = true;
        gameItem.anchor.x = gameItem.anchor.y = 0.5;
        gameItem.width = this.containerWidth;
        gameItem.height = this.containerHeight;
        gameItem.x = this.offsetX + gameItem.width / 2;
        gameItem.y = this.offsetY + gameItem.height / 2;
        gameItem.on('pointerdown', () => {
            let actions = new Actions(this);
            actions.click();
        })
        this.gameItem = gameItem;
        container.addChild(gameItem);
    }

    getPixiItemInstanse(): object {
        return this.gameItem;
    }

    private getRandomTexture() {
        let colors = ['red', 'purple', 'yellow', 'blue', 'green'];
        let randomNumber = Helper.getRandomNumberForArray(colors.length);
        this.color = colors[randomNumber];
        let texturePath = `/assets/images/${this.color}.png`;
        return PIXI.Texture.from(texturePath);
    }

    delete() {
        this.pixiContainer.removeChild(this.gameItem);
    }
}