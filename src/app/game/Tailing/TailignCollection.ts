import {Tailing} from "./Tailing";

export class TailignCollection {
    private static instance: TailignCollection;
    private collection: Tailing[];

    private constructor() {
        this.collection = [];
    }

    public static getInstance(): TailignCollection {
        if (!TailignCollection.instance) {
            TailignCollection.instance = new TailignCollection();
        }
        return TailignCollection.instance;
    }

    get(): Tailing[] {
        return this.collection;
    }

    add(tailing: Tailing) {
        this.collection.push(tailing);
    }

    remove(tailing: Tailing) {
        let index = this.collection.findIndex((item) => {
            return item.id == tailing.id;
        })
        if (index >= 0) {
            this.collection.splice(index, 1);
        }
    }

}