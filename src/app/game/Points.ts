import {Game} from "./Game";
import * as PIXI from 'pixi.js';
import {GamePlayConfig} from "../config/GamePlayConfig";

export class Points {
    private static instance: Points;
    private points: number;
    private app: any;
    private pointItem: any;

    constructor() {
        this.app = Game.getApp();
        this.points = 0;
    }

    public static getInstance(): Points {
        if (!Points.instance) {
            Points.instance = new Points();
        }
        return Points.instance;
    }

    create() {
        this.points = 0;
        const pointItem = new PIXI.Text(`Очки:  ${this.points}`);
        pointItem.x = 1200;
        pointItem.y = 420;
        this.pointItem = pointItem;
        this.app.stage.addChild(pointItem);
    }

    add(points: number) {
        if (points > (GamePlayConfig.minTailingInGroup + 1)) {
            points = 2 * (points - GamePlayConfig.minTailingInGroup) + points;
        }
        this.points += points;
        this.pointItem.text = `Очки:  ${this.points}`;
    }

    get() {
        return this.points;
    }
}