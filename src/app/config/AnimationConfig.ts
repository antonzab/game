export enum AnimationConfig {
    'animationTailingDropSpeed'=10,
    'animationTailingBuildSpeed' = 5,
    'animationTailingDeleteSpeed' = 15
}