import {CreateAnimation} from "./ProgressBarAnimation/CreateAnimation";
import {ProgressAnimation} from "./ProgressBarAnimation/ProgressAnimation";

export class ProgressBarAnimation {
    constructor() {

    }

    create() {
        new CreateAnimation().animate()
    }

    progress() {
        new ProgressAnimation().animate();
    }
}