import {TailignCollection} from "../../game/Tailing/TailignCollection";
import {TailingAnimation} from "../TailingAnimation";
import {ITailingAnimation} from "../Interfaces/iTailingAnimation";
import {AnimationConfig} from "../../config/AnimationConfig";
import {Tailing} from "../../game/Tailing/Tailing";
import {GamePlayConfig} from "../../config/GamePlayConfig";

export class DropAnimate implements ITailingAnimation {

    private animateEnd: boolean;
    private tailing: Tailing;
    private app: any;

    constructor(tailingAnimation: TailingAnimation) {
        this.animateEnd = false;
        this.tailing = tailingAnimation.tailing;
        this.app = tailingAnimation.app;
    }

    isEndAnimation(): boolean {
        return this.animateEnd;
    }

    animate(): void {
        let isNextPositionNotEmpty = this.isNextPositionNotEmpty();
        let isEndPosition = this.isEndPosition();
        if (isNextPositionNotEmpty && !isEndPosition) {
            this.setTailingPosition();
            this.execute();
        } else if ( !isNextPositionNotEmpty || isEndPosition) {
            this.animateEnd = true;
        }
    }

    private isNextPositionNotEmpty(): boolean {
        let collection = TailignCollection.getInstance().get();
        let searched = collection.findIndex((collectionTailing) => {
            let isXEqual = collectionTailing.coordinate[0] == this.tailing.coordinate[0];
            let isNextYEqaul = collectionTailing.coordinate[1] == this.tailing.coordinate[1] + 1;
            return isXEqual && isNextYEqaul;
        });
        return searched == -1;
    }

    private isEndPosition() {
        return this.tailing.coordinate[1] == (GamePlayConfig.gamePlaceSizeHeight - 1);
    }

    private execute() {
        let gameItem: any = this.tailing.getPixiItemInstanse();
        let startPosition = gameItem.y;
        let gameItemAnimation = () => {
            let distance = gameItem.y - startPosition + AnimationConfig.animationTailingDropSpeed;
            if (distance >= gameItem.height) {
                gameItem.y = startPosition + gameItem.height;
                this.app.ticker.remove(gameItemAnimation);
                this.animate();
            } else {
                gameItem.y += AnimationConfig.animationTailingDropSpeed;
            }
        };
        this.app.ticker.add(gameItemAnimation);
    }

    private setTailingPosition() {
        this.tailing.coordinate[1]++;
    }
}
