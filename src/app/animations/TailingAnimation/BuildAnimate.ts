import {ITailingAnimation} from "../Interfaces/iTailingAnimation";
import {TailingAnimation} from "../TailingAnimation";
import {AnimationConfig} from "../../config/AnimationConfig";

export class BuildAnimate  implements ITailingAnimation{

    private isRunedAnimation: boolean;
    private tailingAnimation: TailingAnimation;

    constructor(tailngAnimation: TailingAnimation){
        this.isRunedAnimation = false;
        this.tailingAnimation = tailngAnimation;
    }

    animate(): void {
        let gameItem: any = this.tailingAnimation.tailing.getPixiItemInstanse();
        let size = gameItem.width;
        gameItem.width = gameItem.height =  0;
        if (!this.isRunedAnimation ) {
            this.isRunedAnimation = true;
            let gameItemAnimation = () => {
                if (size <= gameItem.width) {
                    gameItem.width = gameItem.height = Math.round(gameItem.height);
                    this.tailingAnimation.app.ticker.remove(gameItemAnimation);
                    this.isRunedAnimation = false;
                } else {
                    gameItem.width = gameItem.height += size / AnimationConfig.animationTailingBuildSpeed;
                }
            }
            this.tailingAnimation.app.ticker.add(gameItemAnimation)
        }
    }

    isEndAnimation(): boolean {
        return !this.isRunedAnimation;
    }
}