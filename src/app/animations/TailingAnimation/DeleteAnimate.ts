import {ITailingAnimation} from "../Interfaces/iTailingAnimation";
import {TailingAnimation} from "../TailingAnimation";
import {AnimationConfig} from "../../config/AnimationConfig";

export class DeleteAnimate implements ITailingAnimation {

    private isRunedAnimation: boolean;
    private tailingAnimation: TailingAnimation;

    constructor(tailngAnimation: TailingAnimation){
        this.isRunedAnimation = false;
        this.tailingAnimation = tailngAnimation;
    }

    animate() {
        let gameItem: any = this.tailingAnimation.tailing.getPixiItemInstanse();
        if (!this.isRunedAnimation) {
            this.isRunedAnimation = true;
            let gameItemAnimation = () => {
                if (gameItem.width <= 10) {
                    this.tailingAnimation.tailing.delete();
                    this.tailingAnimation.app.ticker.remove(gameItemAnimation);
                    this.isRunedAnimation = false;
                } else {
                    gameItem.width = gameItem.height -= AnimationConfig.animationTailingDeleteSpeed;
                }
            }
            this.tailingAnimation.app.ticker.add(gameItemAnimation)
        }
    }

    isEndAnimation(): boolean {
        return !this.isRunedAnimation;
    }
}
