import {Tailing} from "../game/Tailing/Tailing";
import {Game} from "../game/Game";
import {DropAnimate} from "./TailingAnimation/DropAnimate";
import {BuildAnimate} from "./TailingAnimation/BuildAnimate";
import {DeleteAnimate} from "./TailingAnimation/DeleteAnimate";

export class TailingAnimation {
    readonly app: any;
    readonly tailing: Tailing;

    constructor(tailing: Tailing) {
        this.app = Game.getApp();
        this.tailing = tailing;
    }

    drop(){
        const dropAnimate = new DropAnimate(this);
        dropAnimate.animate();
        let currentYPosition = this.tailing.coordinate[1];
        return new Promise( (resolve) => {
            let interval = setInterval(()=>{
                if(dropAnimate.isEndAnimation()) {
                    clearInterval(interval);
                    resolve(currentYPosition != this.tailing.coordinate[1]);
                }
            }, 50);
        });
    }

    build() {
        const buildAnimate = new BuildAnimate(this);
        buildAnimate.animate();
        return new Promise( (resolve) => {
            let interval = setInterval(()=>{
                if(buildAnimate.isEndAnimation()) {
                    clearInterval(interval);
                    resolve(true);
                }
            }, 50);
        });
    }

    delete() {
        const deleteAnimate = new DeleteAnimate(this);
        deleteAnimate.animate();
        return new Promise( (resolve) => {
            let interval = setInterval(()=>{
                if(deleteAnimate.isEndAnimation()) {
                    clearInterval(interval);
                    resolve(true);
                }
            }, 50);
        });
    }
}