export interface ITailingAnimation {
    animate():void;
    isEndAnimation():boolean;
}
