import {State} from "../State";
import {Scene} from "../Scene";
import {GameScene} from "./GameScene";

export class StartScene extends State {
    run() {
        alert('Вы готов дети?');
        const scene = new Scene(new GameScene());
        scene.run();
    }
}