import {State} from "../State";
import {Scene} from "../Scene";
import {GameScene} from "./GameScene";

export class EndScene extends State {
    run(): void {
        alert('Время вышло. Вы проиграли! Еще разок?');
        const scene = new Scene(new GameScene());
        scene.run();
    }

}