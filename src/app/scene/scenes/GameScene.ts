import {State} from "../State";
import {Game} from "../../game/Game";
import {GamePlace} from "../../game/GamePlace/GamePlace";
import {TailingFactory} from "../../game/Tailing/TailingFactory";
import {Points} from "../../game/Points";
import {ProgressBar} from "../../game/ProgressBar";
import {Time} from "../../game/Time";
import {Scene} from "../Scene";
import {EndScene} from "./EndScene";
import {WinScene} from "./WinScene";

export class GameScene extends State {
    run() {
        new Game().init();
        new GamePlace().create();

        Points.getInstance().create();

        const progressBar = ProgressBar.getInstance();
        progressBar.create(300);

        const time = Time.getInstance();
        time.setTime(60);
        time.create();
        time.run();

        const tailingFactory = new TailingFactory();
        tailingFactory.generate();

        progressBar.listener().then(()=>{
            const scene = new Scene(new WinScene());
            scene.run();
        });

        time.listener().then(()=>{
            const scene = new Scene(new EndScene());
            scene.run();
        })


    }
}