import {State} from "./State";

export class Scene {

    private state: State;

    constructor(state: State) {
        this.transitionTo(state);
    }

    public transitionTo(state: State): void {
        this.state = state;
        this.state.setScene(this);
    }

    public run(): void {
        this.state.run();
    }
}