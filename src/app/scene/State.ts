import {Scene} from "./Scene";

export abstract class State {
    protected scene: Scene;

    public setScene(scene: Scene) {
        this.scene = scene;
    }

    public abstract run(): void;
}