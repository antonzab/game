import {Tailing} from "../game/Tailing/Tailing";
import {GroupTailing} from "../game/Tailing/GroupTailing";
import {Points} from "../game/Points";
import {FillSpace} from "../game/FillSpace";

export class Actions {

    private tailing: Tailing;
    static lock: boolean = false;

    constructor(tailing: Tailing) {
        this.tailing = tailing;
    }

    click(): boolean {
        if(Actions.lock) {
            return false
        }
        Actions.lock = true
        let group = new GroupTailing(this.tailing);
        group.delete().then(()=>{
            Points.getInstance().add(group.getGroupSize());
            new FillSpace().execute().then(()=> {
                Actions.lock = false
            });
        });

        return true;
    }
}