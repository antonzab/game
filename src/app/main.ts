import { Scene } from './scene/Scene';
import { StartScene } from './scene/scenes/StartScene';

const scene = new Scene(new StartScene());
scene.run();